# CryptoFireCloud

Cryptofirecloud is a web application that implements a key management system that allows for the use of a secure cloud storage application. Cryptofirecloud is written in HTML, CSS and Javascript using the Vue 3 framework and Element Plus UI library. The backend is a Firebase project, where the NoSQL database, the authentication system, and cloud storage reside. The application makes use of the open-source libraries crypto-js and hybrid-crypto-js to perform encryption, decryption and key generation.

![Registration](images/home.png)

**User registration**

Every user must register with an email and password. Upon registration, an RSA 4096-bit public-private key pair is generated for them. The user’s private key is then encrypted in the browser with AES encryption, using their password as the key. The public key and the encrypted private key are then stored on Firebase. This is done so that the server never has access to any private keys, so the server owner can never access any of the stored files, and the users don’t have to worry about data breaches.

![Registration](images/registration.png)

**User login**

When a user logs in, their public key and their encrypted private key are sent to the browser from the server. Their private key is then decrypted by the browser using their password, and kept in session storage for use in the rest of the application. Upon logging out or re-opening the browser this is cleared.

![Registration](images/login.png)

**Group creation**

A group may be created by any logged-in user. Upon creation, an RSA 4096-bit public-private key pair for the group is generated in the browser. The private key is then encrypted using the user’s public key. The group public key and the group encrypted private key are then stored on Firebase.

![Registration](images/group_creation.png)

**Adding a user to a group**

Any group member may add other registered users to a group they are in. When they do so, the server will send the browser the group’s encrypted private key. The browser then decrypts the key using the user’s private key. This key is then re-encrypted using the added-user’s public key, and finally stored on Firebase along the rest of the encrypted private keys for the group.

![Registration](images/add_user.png)

**Uploading a file**

A user must first select a group and then upload a file to it. The file is encrypted in the browser using the group’s public key, before being sent to Firebase.

![Registration](images/upload.png)

**Downloading a file**

Any member of a group can see all the files uploaded to a group they’re in. When they choose to download a file, the encrypted file is sent to the browser from the server. Then, the server also sends the group’s private key that was encrypted by that user’s public key. The browser then decrypts the group’s private key using the user’s private key, that was kept in session storage when they logged in. The browser then decrypts the file using the group’s private key, and finally the file is downloaded.

**Removing a user from a group**

When removing a user from a group, their encrypted group private key is simply removed from the server. As they no longer have access to the private key, they will no longer be able to decrypt any of the files in the group. Furthermore, the authentication also makes sure that groups the user is not in are not visible anyway.


**Deleting a file**

Deleting a file simply removes the file from Firebase. Because the application uses file-level encryption, there is no need to decrypt the file before deleting it. Only group members may delete a file.

**Deleting a group**

When a group is deleted, all of the files in the group are deleted as outlined above. Additionally, the entire collection of public and private keys for that group is deleted. This ensures there is no trace of the group left.

---------

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
