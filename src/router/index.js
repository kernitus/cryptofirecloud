import {createRouter, createWebHistory} from "vue-router"

const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import('@/views/Login')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/views/Registration')
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('@/views/Home')
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router